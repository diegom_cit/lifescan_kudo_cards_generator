<?php

class DatabaseConstants {
    const USER_UID_ANONYMOUS = 0;
    const USER_UID_ADMIN = 1;
    const ROLE_RID_ANONYMOUS_USER = 1;
    const ROLE_RID_AUTHENTICATED_USER = 2;
    const ROLE_RID_ADMINISTRATOR = 3;
}