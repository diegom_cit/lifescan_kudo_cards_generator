<?php

class FileConstants {
  const FILE_ALLOWED_EXTENSIONS = 'png gif jpg jpeg';
  const FILE_SIZE_5MB = 5*1024*1024;
}
