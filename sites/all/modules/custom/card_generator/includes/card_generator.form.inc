<?php

## FILE INCLUDES ##
require_once(__DIR__ . '/../constants/DatabaseConstants.php');
require_once(__DIR__ . '/../constants/FileConstants.php');
require_once(__DIR__ . '/../constants/FormAPIConstants.php');
require_once(__DIR__ . '/card_generator.db.inc');
require_once(__DIR__ . '/card_generator.utils.inc');
## FILE INCLUDES ##

function card_generator_build($form, &$form_state) {
  global $user;
  $user_from = user_load($user->uid);
  $default_from = array($user_from->uid => _card_generator_get_user_first_last_name($user_from));

  $usersTo = _card_generator_fetch_users_to($user_from->uid);
  $types  = _card_generator_get_taxonomy_options('field_kudo_type');
  $formats = _card_generator_get_taxonomy_options('field_kudo_format');
  $logos  = _card_generator_get_taxonomy_options('field_kudo_logo');

  if (!isset($usersTo)) {
    drupal_set_message(t('There are no registered users.'), 'warning');
  }

  $items = array(
    '#tree' => TRUE,
    '#attached' => array(
      'js' => array(
        array(
          'type' => 'setting',
          'data' => array(
            'types' => _card_generator_get_taxonomy_terms($types),
            'formats' => _card_generator_get_taxonomy_terms($formats),
            'logos' => _card_generator_get_taxonomy_terms($logos),
            'image_placeholder' => drupal_get_path('module', 'card_generator') . '/images/image_placeholder.png',
          ),
        ),
        drupal_get_path('module', 'card_generator') . '/js/form.js',
      ),
      'css' => array(
        drupal_get_path('module', 'card_generator') . '/css/form.css',
        drupal_get_path('module', 'card_generator') . '/css/card.css',
      ),
    ),
  );

  $items['container_form'] = array(
    '#prefix' => '<div class="row display">',
    '#suffix' => '</div>',
  );

  $items['container_form']['card_info'] = array(
    '#type' => 'fieldset',
    '#disabled' => !isset($usersTo),
    '#attributes' => array('class' => array('col-md-6')),
  );

  $items['container_form']['card_info']['custom_chk'] = array(
    '#type' => 'checkbox',
    '#title' => t('Custom Card?'),
  );

  $items['container_form']['card_info']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#states' => array(
      'visible' => array(
        ':input[name="container_form[card_info][custom_chk]"]' => array(
          'checked' => TRUE,
        ),
      ),
      'required' => array(
        ':input[name="container_form[card_info][custom_chk]"]' => array(
          'checked' => TRUE,
        ),
      ),
    ),
    '#parents' => array('container_form','card_info', 'custom_properties', 'title'),
    '#default_value' => 'Your Custom Title',
  );

  $items['container_form']['card_info']['type'] = array(
    '#type' => 'select',
    '#title' => t('Choose the card type'),
    '#options' => $types,
    '#states' => array(
      'visible' => array(
        ':input[name="container_form[card_info][custom_chk]"]' => array(
          'checked' => FALSE,
        ),
      ),
    ),
  );

  $items['container_form']['card_info']['from'] = array(
    '#type' => 'select',
    '#title' => t('From'),
    '#default_value' => $user_from->uid,
    '#options' => $default_from,
    '#disabled' => TRUE,
  );

  $items['container_form']['card_info']['to'] = array(
    '#type' => 'select',
    '#title' => t('To'),
    '#options' => $usersTo ?? array('None',),
  );

  $items['container_form']['card_info']['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Your Message'),
    '#required' => TRUE,
  );

  $items['container_form']['card_info']['format'] = array(
    '#type' => 'select',
    '#title' => t('Choose a format'),
    '#options' => $formats,
  );

  $items['container_form']['card_info']['logo'] = array(
    '#type' => 'select',
    '#title' => t('Choose a logo'),
    '#options' => $logos,
  );

  $items['container_form']['card_info']['custom_properties'] = array(
    '#type' => 'container',
    'color' => array(
      '#type' => 'jquery_colorpicker',
      '#title' => t('Choose a color'),
    ),
    'image' => array(
      '#type' => 'media',
      '#title' => t('Choose an image'),
      '#description' => t('Only the following file extensions are allowed: %extensions', array('%extensions' => FileConstants::FILE_ALLOWED_EXTENSIONS)),
      '#media_options' => array(
        'global' => array(
          'file_directory' => 'kudos_images',
          'file_extensions' => FileConstants::FILE_ALLOWED_EXTENSIONS,
          'uri_scheme' => 'public',
          'types' => array('image'),
        ),
      ),
    ),
    '#states' => array(
      'visible' => array(
        ':input[name="container_form[card_info][custom_chk]"]' => array(
          'checked' => TRUE,
        ),
      ),
    ),
  );

  $items['container_form']['kudo_container'] = array(
    '#theme' => 'kudo_card',
  );

  $items['container_submit'] = array(
    '#prefix' => '<div class="row">',
    '#suffix' => '</div>',
  );

  $items['container_submit']['buttons'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('col-md-12')),
    'submit' => array (
      '#type' => 'submit',
      '#value' => t('Submit'),
      '#disabled' => !isset($usersTo),
    ),
    'cancel' => array(
      '#type' => 'submit',
      '#value' => t('Cancel'),
      '#submit' => array('card_generator_cancel'),
      '#limit_validation_errors' => array(),
      '#validate' => array(),
      '#attributes' => array('class' => array('btn-danger')),
    ),
  );

  return $items;
}

function card_generator_build_validate($form, &$form_state) {
  $card_info = $form_state['values']['container_form']['card_info'];
  if ($card_info['custom_chk'] === FormAPIConstants::CHECKBOX_CHECKED) {
    if (empty($card_info['custom_properties']['title'])) {
      form_set_error('container_form][card_info][custom_properties][title', t('The Title field is required for a custom card.'));
    }
    if (empty($card_info['custom_properties']['color'])) {
      form_set_error('container_form][card_info][custom_properties][color', t('The Color field is required for a custom card.'));
    }
    if (empty($card_info['custom_properties']['image'])) {
      form_set_error('container_form][card_info][custom_properties][image', t('An image is required for a custom card.'));
    }
  }
  if ($card_info['from'] === $card_info['to']) {
    form_set_error('container_form][card_info][to', t('You can\'t write the card to yourself.'));
  }
  if ($card_info['to'] == DatabaseConstants::USER_UID_ANONYMOUS) {
    form_set_error('container_form][card_info][to', t('There is no one but you in the system.'));
  }
}

function card_generator_build_submit($form, &$form_state) {
  global $user;
  $kudo_card_wrapper = _card_generator_create_card($form_state);
  $url_path = 'node/' . $kudo_card_wrapper->getIdentifier();
  $url_alias = drupal_get_path_alias($url_path);
  $form_state['redirect'] = $url_alias;
}

function card_generator_cancel($form, &$form_state) {
  drupal_set_message(t('You have canceled the card\'s generation'), 'warning');
  $form_state['redirect'] = '';
}


