<?php

function _card_generator_get_user_first_last_name($user) {
  $wrapper = entity_metadata_wrapper('user', $user);
  return $wrapper->field_first_name->value().' '.$wrapper->field_last_name->value();
}

function _card_generator_is_administrator($user) {
  return isset($user->roles[DatabaseConstants::ROLE_RID_ADMINISTRATOR]);
}

function _card_generator_is_checked($checkbox_field) {
  return $checkbox_field == FormAPIConstants::CHECKBOX_CHECKED;
}
