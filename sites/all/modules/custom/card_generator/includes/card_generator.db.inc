<?php

/**
 * Retrieves the authenticated users from the Database.
 *
 * The currently logged in user and administrator users are NOT fetched.
 *
 * @param int $logged_uid
 * @return array
 */
function _card_generator_fetch_users_to($logged_uid) {
  $users = NULL;

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'user')
    ->entityCondition('entity_id',
        array(DatabaseConstants::USER_UID_ANONYMOUS,
              $logged_uid),
        'NOT IN')
    ->addMetaData('account', user_load(DatabaseConstants::USER_UID_ADMIN));

  $result = $query->execute();

  if (!empty($result)) {
    $uids = array_keys($result['user']);
    $dbUsers = entity_load('user', $uids);

    foreach ($dbUsers as $dbUser) {
      if (!_card_generator_is_administrator($dbUser)) {
        $users[$dbUser->uid] = _card_generator_get_user_first_last_name($dbUser);
      }
    }
  }

  return $users;
}

function _card_generator_get_taxonomy_options($field_name) {
  $field = field_info_field($field_name);
  return taxonomy_allowed_values($field);
}

function _card_generator_get_taxonomy_terms($array) {
  $tids = array_keys($array);
  return _card_generator_build_settings_array(entity_load('taxonomy_term', $tids));
}

function _card_generator_build_settings_array($entities) {
  $array = NULL;

  foreach ($entities as $key => $entity) {
    $wrapper = entity_metadata_wrapper('taxonomy_term', $entity);
    $id = $wrapper->getIdentifier();

    foreach ($wrapper->getPropertyInfo() as $key => $value) {
      $array[$id][$key] = $wrapper->$key->value();

      if (is_array($array[$id][$key]) && array_key_exists('uri', $array[$id][$key])) {
        $array[$id][$key]['url'] = file_create_url($array[$id][$key]['uri']);
      }
    }
  }

  return $array;
}

function _card_generator_create_card($form_state) {
  global $user, $language;
  $card_info = $form_state['values']['container_form']['card_info'];

  $entity = entity_create('node', array('type' => 'kudo_card'));
  $entity->uid = $user->uid;
  $entity->language = $language->language;

  $wrapper = entity_metadata_wrapper('node', $entity);
  $wrapper = _card_generator_set_wrapper_fields($wrapper, $card_info);
  $wrapper->save();

  if (_card_generator_is_checked($card_info['custom_chk'])) {
    saveImage($card_info['custom_properties']['image'], $wrapper->getIdentifier());
  }

  return $wrapper;
}

function _card_generator_set_wrapper_fields($wrapper, $card_info) {
  if (_card_generator_is_checked($card_info['custom_chk'])) {
    $wrapper->title = check_plain($card_info['custom_properties']['title']);
    $wrapper->field_kudo_color = $card_info['custom_properties']['color'];
    $title_alt = 'Custom "' . $wrapper->title->value() . '" card\'s image.';
    $wrapper->field_kudo_image = array(
      'fid' => $card_info['custom_properties']['image'],
      'title' => $title_alt,
      'alt' => $title_alt,
    );
  }
  else {
    $wrapper->field_kudo_type = $card_info['type'];
    $type_wrapper = entity_metadata_wrapper('taxonomy_term', $wrapper->field_kudo_type->getIdentifier());
    $wrapper->title = $type_wrapper->label();
    $wrapper->field_kudo_color = $type_wrapper->field_kudo_type_color->value();
    $wrapper->field_kudo_image = $type_wrapper->field_kudo_type_image->value();
  }

  $wrapper->field_kudo_from = $card_info['from'];
  $wrapper->field_kudo_to = $card_info['to'];
  $wrapper->field_kudo_message->value = $card_info['message'];
  $wrapper->field_kudo_format = $card_info['format'];
  $wrapper->field_kudo_logo = $card_info['logo'];

  return $wrapper;
}

function saveImage($fid, $nid) {
  $file = file_load($fid);
  $file->status = FILE_STATUS_PERMANENT;
  file_save($file);
  file_usage_add($file, 'card_generator', 'kudo_card', $nid);
}
