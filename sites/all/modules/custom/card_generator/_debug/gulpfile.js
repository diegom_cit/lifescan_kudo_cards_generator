
const gulp = require("gulp");
const sass = require('gulp-sass');

var paths = {
    sass: 'sass/**/*.scss',
    css: '../css'
};

gulp.task('css', function () {
    return gulp
        .src(paths.sass)
        .pipe(sass())
        .pipe(gulp.dest(paths.css))
});

gulp.task('watch', function () {
    gulp.watch(paths.sass, gulp.series('css'));
});

gulp.task('default', gulp.series('watch'));