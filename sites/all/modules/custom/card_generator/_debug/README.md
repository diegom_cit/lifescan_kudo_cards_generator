# GULP (Yep, it's better than GRUNT)

## Pre-requisites

Be sure to use **npm 6.4.1**.

## Instalation

Install cli for gulp 4 globaly:

`
$ npm install gulpjs/gulp-cli -g
`

Init the npm on this folder:

`
$ npm init
`

## Usage

`
$ gulp
`