(function ($) {
  Drupal.behaviors.generateKudoCard = {
    attach: function(context, settings) {
      var formType = $('#edit-container-form-card-info-type', context),
          formFrom = $('#edit-container-form-card-info-from', context),
          formTo = $('#edit-container-form-card-info-to', context),
          formTitle = $('#edit-container-form-card-info-custom-properties-title', context),
          formColor = $('div.color_picker', context),
          formText = $('#edit-container-form-card-info-message', context),
          formFormat = $('#edit-container-form-card-info-format', context),
          formLogo = $('#edit-container-form-card-info-logo', context),
          formInputColorPicker = $('.form-colorpicker', context),
          checkbox = $('#edit-container-form-card-info-custom-chk', context),
          kudoHeader = $('#kudo-header', context),
          kudoCard = $('#kudo-card', context),
          kudoTitle = $('#kudo-title', context),
          kudoFrom = $('#kudo-from', context),
          kudoTo = $('#kudo-to', context),
          kudoMessage = $('#kudo-message', context),
          kudoImageTop = $('img.panel-img-top', context),
          kudoImageBottom = $('img.panel-img-bottom', context);
          customImagePreview = $('.media-widget div.preview > div img'),
          targetNodes         = $('#edit-container-form-card-info-custom-properties-image-upload--widget', context),
          MutationObserver    = window.MutationObserver || window.WebKitMutationObserver,
          myObserver          = new MutationObserver(mutationHandler),
          obsConfig           = {childList: true, subtree: true},
          colorPickerColor = formColor.css('background-color');

      function init() {
        setCardDefaultValues();
        colorPickerColor = getDrupalSetting('types', formType, 'field_kudo_type_color');
      }

      function setCardDefaultValues() {
        var cardColor = getDrupalSetting('types', formType, 'field_kudo_type_color'),
            imageTopUrl = getDrupalSetting('types', formType, 'field_kudo_type_image').url,
            imageBottomUrl = getDrupalSetting('logos', formLogo, 'field_kudo_logo_image').url;

        setElementAttribute(kudoImageTop, 'src', imageTopUrl);
        setElementAttribute(kudoImageBottom, 'src', imageBottomUrl);
        setElementHtmlFromReference(kudoTitle, formType);
        setElementHtmlFromReference(kudoFrom, formFrom);
        setElementHtmlFromReference(kudoTo, formTo);
        setElementTextFromReference(kudoMessage, formText);
        addClassToElement(kudoCard, getDrupalSetting('formats', formFormat, 'field_kudo_format_css_classes'));
        updateKudoColors(cardColor);
      }

      function updateCard() {
        setCardDefaultValues();

        if (checkbox.is(':checked')) {
          setElementTextFromReference(kudoTitle, formTitle);
          updateKudoCustomImage();
          updateKudoColors(colorPickerColor, true);
        }
      }

      function updateKudoColors(color, rgb = false) {
        if (color) {
          color = rgb ? rgb2hex(color) : '#' + color;

          setElementCss(kudoHeader, 'background-color', color);
          setElementCss(formColor, 'background-color', color);
          formColor.ColorPickerSetColor(color);
        }
      }

      function updateKudoCustomImage() {
        if (customImagePreview.attr('src')) {
          var url = customImagePreview.attr('src').replace('/styles/media_thumbnail/public', '');
          setElementAttribute(kudoImageTop, 'src', url);
        }
        else {
          setElementAttribute(kudoImageTop, 'src', getDrupalSetting('image_placeholder'));
        }
      }

      function updateKudoColors(color, rgb = false) {
        color = rgb ? rgb2hex(color) : '#' + color;

        setElementCss(kudoHeader, 'background-color', color);
        setElementCss(formColor, 'background-color', color);
        formColor.ColorPickerSetColor(color);
        formInputColorPicker.val(color.replace('#', ''));
      }

      function mutationHandler(mutationRecords) {
        for (var i = mutationRecords.length - 1; i >= 0; i--) {
          if (mutationRecords[i].removedNodes.length && i == 0) {
            updateKudoCustomImage();
          }
        }
      }

      targetNodes.each(function () {
        myObserver.observe(this, obsConfig);
      });

      if (arguments[0] === document) {
        if (!$('.alert.error').length) {
          init();
        }
        else {
          updateCard();
        }
      }

      formType.change(function () {
        updateCard();
      });

      formFrom.change(function () {
        setElementHtmlFromReference(kudoFrom, formFrom);
      });

      formTo.change(function () {
        setElementHtmlFromReference(kudoTo, formTo);
      });

      checkbox.change(function () {
        updateCard();
      });

      formTitle.keyup(function () {
        setElementTextFromReference(kudoTitle, formTitle);
      });

      formColor.change(function () {
        var color = formColor.css('background-color');
        setElementCss(kudoHeader, 'background-color', color);
        colorPickerColor = color;
      });

      formText.keyup(function () {
        setElementTextFromReference(kudoMessage, formText);
      });

      formFormat.change(function () {
        kudoCard.removeClass(function (index, css) {
          return (css.match (/\bformat-\S+/g) || []).join(''); // removes anything that starts with 'format-'
        });
        addClassToElement(kudoCard, getDrupalSetting('formats', formFormat, 'field_kudo_format_css_classes'));
      });

      formLogo.change(function () {
        setElementAttribute(kudoImageBottom, 'src', getDrupalSetting('logos', formLogo, 'field_kudo_logo_image').url);
      });
    }
  };

  function getDrupalSetting(settingName, element, fieldName) {
    if (element) {
      return Drupal.settings[settingName][element.children('option:selected').val()][fieldName];
    }
    else {
      return Drupal.settings[settingName];
    }
  }

  function setElementHtmlFromReference(element, reference) {
    element.html(reference.children('option:selected').text());
  }

  function setElementTextFromReference(element, reference) {
    element.text(reference.val());
  }

  function setElementAttribute(element, attribute, value) {
    element.attr(attribute, value);
  }

  function setElementCss(element, property, value) {
    element.css(property, value);
  }

  function addClassToElement(element, value) {
    element.addClass(value);
  }

  function rgb2hex(rgb) {
    if (rgb.search("rgb") == -1) {
      return rgb;
    } else {
      rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);
      function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
      }
      return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
    }
  }
}(jQuery));
