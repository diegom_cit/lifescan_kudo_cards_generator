(function ($) {
  Drupal.behaviors.downloadCard = {
    attach: function (context, settings) {
      var downloadButton = $('#download-kudo'),
          kudoCard = document.getElementById('kudo-card');

      downloadButton.click(function () {
        printToFile(kudoCard);
      });

      function printToFile(div) {
        html2canvas(div, { backgroundColor: null }).then(canvas => {
          var myImage = canvas.toDataURL("image/png");
          downloadURI("data:" + myImage, "Kudo_Card.png");
        });
      }

      function downloadURI(uri, name) {
        var link = document.createElement("a");
        link.download = name;
        link.href = uri;
        link.click();
      }
    }
  };
}(jQuery));
