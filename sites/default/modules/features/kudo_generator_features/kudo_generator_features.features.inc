<?php

/**
 * @file
 * kudo_generator_features.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function kudo_generator_features_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function kudo_generator_features_node_info() {
  $items = array(
    'kudo_card' => array(
      'name' => t('Kudo Card'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Your Kudo'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
