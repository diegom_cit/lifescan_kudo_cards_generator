<?php

/**
 * @file
 * kudo_generator_features.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function kudo_generator_features_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access media browser'.
  $permissions['access media browser'] = array(
    'name' => 'access media browser',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'media',
  );

  // Exported permission: 'create kudo_card content'.
  $permissions['create kudo_card content'] = array(
    'name' => 'create kudo_card content',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any kudo_card content'.
  $permissions['delete any kudo_card content'] = array(
    'name' => 'delete any kudo_card content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own kudo_card content'.
  $permissions['delete own kudo_card content'] = array(
    'name' => 'delete own kudo_card content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any kudo_card content'.
  $permissions['edit any kudo_card content'] = array(
    'name' => 'edit any kudo_card content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own kudo_card content'.
  $permissions['edit own kudo_card content'] = array(
    'name' => 'edit own kudo_card content',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'node',
  );

  return $permissions;
}
